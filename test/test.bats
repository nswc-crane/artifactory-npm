#!/usr/bin/env bats
#
# Test Artifactory NPM
#

set -e

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/artifactory-npm"}
  run docker build -t ${DOCKER_IMAGE} .
  suffix=$(date -u "+%Y%m%d%H%M%S")
  sed -i -e "s/1.0.0/1.0.0-$suffix/g" test/package.json
  cd test
}

@test "Install NPM dependencies from Artifactory" {

  # execute tests
  run docker run \
    -e ARTIFACTORY_URL=$ARTIFACTORY_URL \
    -e ARTIFACTORY_USER=$ARTIFACTORY_USER \
    -e ARTIFACTORY_PASSWORD=$ARTIFACTORY_PASSWORD \
    -e NPM_COMMAND="install" \
    -e NPM_SOURCE_REPO=$NPM_SOURCE_REPO \
    -e BUILD_NAME=$BITBUCKET_REPO_OWNER-$BITBUCKET_REPO_SLUG-$BITBUCKET_BRANCH \
    -e BITBUCKET_BUILD_NUMBER=$BITBUCKET_BUILD_NUMBER \
    -e COLLECT_GIT_INFO="false" \
    -e DEBUG="true" \
    -v $(pwd):$(pwd) \
    -w $(pwd) \
  ${DOCKER_IMAGE}

  echo ${output}

  [[ "${status}" == "0" ]]

}

@test "Publish NPM package to Artifactory" {

  # execute tests
  run docker run \
    -e ARTIFACTORY_URL=$ARTIFACTORY_URL \
    -e ARTIFACTORY_USER=$ARTIFACTORY_USER \
    -e ARTIFACTORY_PASSWORD=$ARTIFACTORY_PASSWORD \
    -e NPM_COMMAND="publish" \
    -e NPM_TARGET_REPO=$NPM_SOURCE_REPO \
    -e BUILD_NAME=$BITBUCKET_REPO_OWNER-$BITBUCKET_REPO_SLUG-$BITBUCKET_BRANCH \
    -e BITBUCKET_BUILD_NUMBER=$BITBUCKET_BUILD_NUMBER \
    -e COLLECT_GIT_INFO="false" \
    -e DEBUG="true" \
    -v $(pwd):$(pwd) \
    -w $(pwd) \
  ${DOCKER_IMAGE}

  echo ${output}
  
  [[ "${status}" == "0" ]]

}
